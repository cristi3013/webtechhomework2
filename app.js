function addTokens(input, tokens) {
  if (typeof input === 'string' || input instanceof String) {
  } else {
    throw 'Invalid input';
  }

  if (input.length < 6) {
    throw 'Input should have at least 6 characters';
  }

  return input;
}

const app = {
  addTokens: addTokens
};

module.exports = app;
